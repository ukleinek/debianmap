#!/usr/bin/env python3

import aiohttp.web
import os.path

rootpath = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'www')

app = aiohttp.web.Application()
app.add_routes([
    aiohttp.web.static('/javascript', '/usr/share/javascript'),
    aiohttp.web.static('/', rootpath)])

aiohttp.web.run_app(app)
