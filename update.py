#!/usr/bin/env python3

import json
import yaml
import os

users = dict()

for root, dirs, files in os.walk('developers'):
    for f in files:
        if not f.endswith('.yaml'):
            continue

        username = f[:-5]
        userinfo = yaml.safe_load(open(os.path.join(root, f)))
        if 'locations' not in userinfo:
            continue

        userinfo['type'] = 'developer'

        if 'email' not in userinfo:
            userinfo['email'] = f'{username}@debian.org'

        locations = userinfo['locations']
        del userinfo['locations']

        for location in locations:
            tlocation = tuple(location['latlng'])
            users.setdefault(tlocation, {'latlng': location['latlng'], 'users': []})
            users[tlocation]['users'].append(userinfo)

for llist in users.values():
    llist['users'].sort(key=lambda u: u['name'] if 'name' in u else '')

json.dump(list(users.values()), open('www/users.json', 'w'))
