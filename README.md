Debian Map
==========

The map shows (for now) Debian members. Personal markers are generated from
yaml files in `developers/`.

Quickstart
----------

To combine all data in the json file that is then used to render the map, run
the `update.py` script:

```console
$ python3 update.py
```

Then either put the files below `www/` on your web server, or use the
`server.py` script:

```console
$ python3 server.py 
```

and access the map at http://localhost:8080/index.html.

Dependencies
------------

```console
# apt install libjs-leaflet python3-yaml python3-aiohttp
```

(`python3-aiohttp` is only needed for the `server.py` script.)

